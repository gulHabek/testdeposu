from nltk.stem import WordNetLemmatizer

from sklearn import model_selection,preprocessing, naive_bayes

from sklearn.model_selection import KFold

from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer

from sklearn.metrics import accuracy_score

from sklearn.metrics import confusion_matrix

import pandas as pd

import numpy as np

"""deneme yazısı"""

dataBitci = pd.read_csv("C:\\Users\\TOSHIBA\\Downloads\\BITCIdata.tsv",sep="\t")
dataBTC=pd.read_csv("C:\\Users\\TOSHIBA\\Downloads\\bitcoinData (1).tsv",sep="\t")

data=pd.concat([dataBitci,dataBTC],axis=0)

data['Sentiment'].replace('0',value='negative',inplace = True)

data['Sentiment'].replace('1',value='positive',inplace = True)


data = data[(data.Sentiment == "negative") | (data.Sentiment == "positive")]

data.groupby("Sentiment").count() # grupları gösteriyor

# metin ve etiketleri bir Dataframe e taşıyoruz
df = pd.DataFrame()
df["text"] = data["Phrase"]
df["label"] = data["Sentiment"]

## Metin önişleme kodları
df['text'] = df['text'].apply(lambda x: " ".join(kelime.lower() for kelime in x.split()))
df['text'] = df['text'].str.replace("[^\w\s]","")
df['text'] = df['text'].str.replace("\d","")

import nltk     
nltk.download("stopwords")
from nltk.corpus import stopwords
stopwords = stopwords.words("turkish")
df['text'] = df['text'].apply(lambda x: " ".join(kelime for kelime in x.split() if kelime not in stopwords))

sil = pd.Series(" ".join(df['text']).split()).value_counts()[-1000:]

df['text'] = df['text'].apply(lambda x: " ".join(kelime for kelime in x.split() if kelime not in sil))

lemma = WordNetLemmatizer()
df['text'] = df['text'].apply(lambda x: " ".join(lemma.lemmatize(kelime) for kelime in x.split()))

##classification model
train_x,test_x,train_y,test_y = model_selection.train_test_split(df["text"],df["label"],test_size=0.1)
encoder = preprocessing.LabelEncoder()
train_y = encoder.fit_transform(train_y)
test_y = encoder.fit_transform(test_y)

#vectorizer = CountVectorizer(max_features = 1000) # TF bu değer 2000,3000 de olabilir dene!
#vectorizer = CountVectorizer(max_features = 1000,binary=True) # TP
vectorizer = TfidfVectorizer(max_features = 1000) #en ityi sonuç veren bu ! bunuda dene karşılaştır !


vectorizer.fit(train_x)   # Learn a vocabulary dictionary of all tokens in the raw documents.
#vectorizer.get_feature_names()[0:5]
allVocabs=vectorizer.get_feature_names()


train_x_tf = vectorizer.transform(train_x) #Transform documents to document-term matrix.
train_x_tf.shape
test_x_tf =  vectorizer.transform(test_x)  # test veri setini DTM'e uyarlandı
test_x_tf.shape


## results.............
#naive bayes
nb = naive_bayes.MultinomialNB()
confusionMatrixScores_nb = np.zeros((2,2))
accuracyScores_nb = []



#logistic regression
from sklearn.linear_model import LogisticRegression
logr = LogisticRegression(random_state=0)
confusionMatrixScores_logr = np.zeros((2,2))
accuracyScores_logr = []


#SVM
from sklearn.svm import SVC
svm = SVC(kernel='linear')
confusionMatrixScores_svm = np.zeros((2,2))
accuracyScores_svm = []


#decisionTree
from sklearn.tree import DecisionTreeClassifier
dt = DecisionTreeClassifier()
confusionMatrixScores_dt = np.zeros((2,2))
accuracyScores_dt = []

#random forest
from sklearn.ensemble import RandomForestClassifier
rf = RandomForestClassifier(n_estimators = 5)
confusionMatrixScores_rf = np.zeros((2,2))
accuracyScores_rf = []


#10 fold cross validation
kf=KFold(n_splits=10,shuffle=True)


for train_index, test_index in kf.split(train_x_tf):
    
    X_train,X_test=train_x_tf[train_index],train_x_tf[test_index]
    y_train,y_test=train_y[train_index],train_y[test_index]
    
    
    #naive_bayes
    nb.fit(X_train, y_train)
    y_pred_nb = nb.predict(X_test)
    confusionMatrixScores_nb = confusionMatrixScores_nb + confusion_matrix(y_test,y_pred_nb)
    accuracyScores_nb.append(accuracy_score(y_test, y_pred_nb))
    
    #logistic regression
    logr.fit(X_train, y_train)
    y_pred_logr = logr.predict(X_test)
    confusionMatrixScores_logr = confusionMatrixScores_logr + confusion_matrix(y_test,y_pred_logr)
    accuracyScores_logr.append(accuracy_score(y_test, y_pred_logr))
    
    #SVM
    svm.fit(X_train, y_train)
    y_pred_svm = svm.predict(X_test)
    confusionMatrixScores_svm = confusionMatrixScores_svm + confusion_matrix(y_test,y_pred_svm)
    accuracyScores_svm.append(accuracy_score(y_test, y_pred_svm))
    
    #decisionTree
    dt.fit(X_train, y_train)
    y_pred_dt = dt.predict(X_test)
    confusionMatrixScores_dt = confusionMatrixScores_dt + confusion_matrix(y_test,y_pred_dt)
    accuracyScores_dt.append(accuracy_score(y_test, y_pred_dt))
    
    
    #random forest
    rf.fit(X_train, y_train)
    y_pred_rf = rf.predict(X_test)
    confusionMatrixScores_rf = confusionMatrixScores_rf + confusion_matrix(y_test,y_pred_rf)
    accuracyScores_rf.append(accuracy_score(y_test, y_pred_rf))
    
'''#decisionTree
print("decision Tree")
print(confusionMatrixScores_dt / 10)
print(sum(accuracyScores_dt) / float(len(accuracyScores_dt)))'''

#naive bayes
print("Naive Bayes")
print(confusionMatrixScores_nb / 10)
print(sum(accuracyScores_nb) / float(len(accuracyScores_nb)))


#logistic regression
print("Logistic Regression")
print(confusionMatrixScores_logr / 10)
print(sum(accuracyScores_logr) / float(len(accuracyScores_logr)))


#support vector machines
print("support vector machines")
print(confusionMatrixScores_svm / 10)
print(sum(accuracyScores_svm) / float(len(accuracyScores_svm)))


#decisionTree
print("decision Tree")
print(confusionMatrixScores_dt / 10)
print(sum(accuracyScores_dt) / float(len(accuracyScores_dt)))

#random forest
print("random forest")
print(confusionMatrixScores_rf / 10)
print(sum(accuracyScores_rf) / float(len(accuracyScores_rf)))


#sonuçları excele yazdırma..........

from openpyxl import Workbook,load_workbook

wb = Workbook()

ws = wb.active
ws.title = "BTC"
#ws = wb.create_sheet("BitciCoin")

ws['A1'] = "Naive Bayes"

ws['A2'] = "Accuracy Score"

ws['B2']=sum(accuracyScores_nb) / float(len(accuracyScores_nb))


ws['A3'] = "Logistic Regression"

ws['A4'] = "Accuracy Score :"

ws['B4']=sum(accuracyScores_logr) / float(len(accuracyScores_logr))


ws['A5'] = "Support Vector Machines"

ws['A6'] = "Accuracy Score :"

ws['B6']=sum(accuracyScores_svm) / float(len(accuracyScores_svm))


ws['A7'] = "Decision Tree"

ws['A8'] = "Accuracy Score :"

ws['B8']=sum(accuracyScores_dt) / float(len(accuracyScores_dt))


ws['A9'] = "Random Forest"

ws['A10'] = "Accuracy Score :"

ws['B10']=sum(accuracyScores_rf) / float(len(accuracyScores_rf))

#ws.append([1, 2, 3, "Merhaba", "Dünya"])  # Sıradaki satıra sırasıyla dizi elemanlarını ekler

print(wb.sheetnames)

wb.save("modelSonuclari.xlsx")



